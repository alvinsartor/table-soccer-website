﻿using System;
using Newtonsoft.Json;

namespace Core
{
    public sealed class Match
    {
        /// <summary>
        /// The match identifier.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// The home team.
        /// </summary>
        public Team HomeTeam { get; }

        /// <summary>
        /// The visitor team.
        /// </summary>
        public Team VisitorTeam { get; }

        /// <summary>
        /// The home score.
        /// </summary>
        public int HomeScore { get; private set; }

        /// <summary>
        /// The visitors score.
        /// </summary>
        public int VisitorScore { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Match"/> is completed.
        /// </summary>
        public bool Completed => HomeScore != -1 && VisitorScore != -1;

        /// <summary>
        /// Gets a value indicating whether this match ended in draw.
        /// </summary>
        public bool IsDraw => HomeScore == VisitorScore;

        /// <summary>
        /// The match date.
        /// </summary>
        public DateTime Date { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Match"/> class.
        /// </summary>
        /// <param name="homeTeam">The home team.</param>
        /// <param name="visitorTeam">The visitor team.</param>
        /// <param name="homeScore">The home team score.</param>
        /// <param name="visitorScore">The visitor team score.</param>
        /// <param name="date">The date.</param>
        public Match(Team homeTeam, Team visitorTeam, int? homeScore, int? visitorScore, DateTime? date)
        {
            Id = Guid.NewGuid().ToString();
            Date = date ?? DateTime.Today;
            HomeTeam = homeTeam;
            VisitorTeam = visitorTeam;
            HomeScore = homeScore ?? -1;
            VisitorScore = visitorScore ?? -1;
        }


        [JsonConstructor]
        private Match(string id, Team homeTeam, Team visitorTeam, int? homeScore, int? visitorScore, DateTime? date)
        {
            Id = id;
            Date = date ?? DateTime.Today;
            HomeTeam = homeTeam;
            VisitorTeam = visitorTeam;
            HomeScore = homeScore ?? -1;
            VisitorScore = visitorScore ?? -1;
        }

        /// <summary>
        /// Ends the match, adding the score.
        /// </summary>
        /// <param name="homeScore">The home score.</param>
        /// <param name="visitorScore">The visitor score.</param>
        public void EndMatch(int homeScore, int visitorScore) =>
            (HomeScore, VisitorScore) = (homeScore, visitorScore);

        /// <summary>
        /// Gets the points for the specified team.
        /// </summary>
        /// <param name="team">The team to check.</param>
        /// <exception cref="ArgumentException">The specified team did not play this match.</exception>
        public MatchResult GetResultFor(Team team)
        {
            if (team.Equals(HomeTeam))
            {
                return HomeScore > VisitorScore
                    ? MatchResult.Victory
                    : HomeScore == VisitorScore
                        ? MatchResult.Draw
                        : MatchResult.Loss;
            }

            if (team.Equals(VisitorTeam))
            {
                return VisitorScore > HomeScore
                    ? MatchResult.Victory
                    : HomeScore == VisitorScore
                        ? MatchResult.Draw
                        : MatchResult.Loss;
            }

            throw new ArgumentException("The specified team did not play this match.");
        }

        /// <summary>
        /// Gets the goals stats for the specified team.
        /// </summary>
        /// <param name="team">The team.</param>
        /// <exception cref="ArgumentException">The specified team did not play this match.</exception>
        public (int received, int scored) GetGoalsStats(Team team)
        {
            return team.Equals(HomeTeam)
                ? (VisitorScore, HomeScore)
                : team.Equals(VisitorTeam)
                    ? (HomeScore, VisitorScore)
                    : throw new ArgumentException("The specified team did not play this match.");
        }

        /// <summary>
        /// Determines whether this instance contains the specified team.
        /// </summary>
        /// <param name="team">The team to check.</param>
        public bool Contains(Team team) => team.Equals(HomeTeam) || team.Equals(VisitorTeam);

        /// <inheritdoc />
        public override bool Equals(object? obj) =>
            obj is Match other
            && Id.Equals(other.Id)
            && HomeTeam.Equals(other.HomeTeam)
            && VisitorTeam.Equals(other.VisitorTeam)
            && HomeScore == other.HomeScore
            && VisitorScore == other.VisitorScore
            && Date.Equals(other.Date);

        /// <inheritdoc />
        public override int GetHashCode() =>
            HashCode.Combine(Id, HomeTeam, VisitorTeam, Date);
    }
}