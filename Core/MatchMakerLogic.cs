﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public static class MatchMakerLogic
    {
        /// <summary>
        /// Creates a list of balanced matches, using the given teams and relative statistics.
        /// </summary>
        /// <param name="teams">The teams.</param>
        public static List<Match> CreateBalancedMatches(Dictionary<Team, TeamStats> teams)
        {
            var unassignedTeams = teams.Keys.ToHashSet();
            var proposedMatches = new List<Match>();

            while (unassignedTeams.Count > 0)
            {
                // popping out first team
                var team = unassignedTeams.First();
                unassignedTeams.Remove(team);

                // finding most similar team from unassigned
                var mostSimilar = FindMostSimilar(team, unassignedTeams, t => teams[t]);

                if (mostSimilar == null)
                {
                    // No team can be matched from the unassigned.
                    // Let's get someone from the entire list then
                    mostSimilar = FindMostSimilar(team, teams.Keys, t => teams[t]);
                }

                if (mostSimilar == null)
                {
                    // No team can be matched.
                    // this team will be skipped.
                    break;
                }

                var proposedMatch = new Match(team, mostSimilar, null, null, null);

                if (proposedMatches.Any(match => match.Contains(team) && match.Contains(mostSimilar)))
                {
                    // The match has already been added.
                    // This team will be skipped.
                }

                proposedMatches.Add(proposedMatch);
                if (unassignedTeams.Contains(mostSimilar)) unassignedTeams.Remove(mostSimilar);
            }

            return proposedMatches;
        }

        private static Team FindMostSimilar(Team single, IEnumerable<Team> collection, Func<Team, TeamStats> getStats)
        {
            var stats = getStats(single);

            return collection.Where(x => !x.HasCommonMembersWith(single))
                .OrderBy(x => getStats(x).GetSimilarity(stats))
                .FirstOrDefault();
        }
    }
}