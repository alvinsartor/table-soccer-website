using NUnit.Framework;

namespace Core.NUnit
{
    [TestFixture]
    internal sealed class MatchResultFixture
    {
        [TestCase(MatchResult.Victory, 2)]
        [TestCase(MatchResult.Draw, 1)]
        [TestCase(MatchResult.Loss, 0)]
        public void PointsCanBeExtractedFromMatchResult(MatchResult result, int expectedPoints)
        {
            Assert.That(result.ToPoints(), Is.EqualTo(expectedPoints));
        }
    }
}