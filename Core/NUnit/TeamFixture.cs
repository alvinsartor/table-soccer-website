using System;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Core.NUnit
{
    [TestFixture]
    internal sealed class TeamFixture
    {
        [Test]
        public void TeamMembersNameIsShown()
        {
            var team = new Team(new[] { new Player("Andonio"), new Player("Zaira") });
            Assert.That(team.MemberNames, Is.EqualTo("Andonio+Zaira"));
        }

        [Test]
        public void TeamCanBeSerializedAndDeserializedBack()
        {
            var team = new Team(new[]{new Player("Andonio")});
            var json = JsonConvert.SerializeObject(team);
            var team2 = JsonConvert.DeserializeObject<Team>(json);

            Assert.That(team, Is.EqualTo(team2));
        }

        [Test]
        public void TooManyOrTooFewMembersCauseException()
        {
            // 0 is definitely not enough
            Assert.Throws<ArgumentException>(() => new Team(new Player[0] { }), "Ghost Team");
            
            // 1 to 3 members are allowed
            Assert.DoesNotThrow(() => new Team(new[] { new Player("Andonio") }), "Team1");
            Assert.DoesNotThrow(() => new Team(new[] { new Player("Andonio"), new Player("Zaira") }), "Team2");
            Assert.DoesNotThrow(() => new Team(new[] { new Player("Andonio"), new Player("Zaira"), new Player("Luca") }), "Team3");
            
            // 4 is too many
            Assert.Throws<ArgumentException>(() =>
                new Team(new[] { new Player("A"), new Player("B"), new Player("C"), new Player("D") }), "Busy Team");
        }

        [Test]
        public void IsItPossibleToSeeIfTeamsShareMembers()
        {
            var andonio = new Player("Andonio");
            var zaira = new Player("Zaira");

            var teamAndonio = new Team(new[] { andonio });
            var teamAndonioZaira = new Team(new[] { andonio, zaira });
            var teamZaira = new Team(new[] { zaira });

            Assert.True(teamAndonio.HasCommonMembersWith(teamAndonioZaira));
            Assert.True(teamAndonioZaira.HasCommonMembersWith(teamAndonio));
                   
            Assert.True(teamZaira.HasCommonMembersWith(teamAndonioZaira));
            Assert.True(teamAndonioZaira.HasCommonMembersWith(teamZaira));
                   
            Assert.True(teamAndonioZaira.HasCommonMembersWith(teamAndonioZaira));
            Assert.True(teamAndonio.HasCommonMembersWith(teamAndonio));
            Assert.True(teamZaira.HasCommonMembersWith(teamZaira));

            Assert.False(teamAndonio.HasCommonMembersWith(teamZaira));
            Assert.False(teamZaira.HasCommonMembersWith(teamAndonio));
        }
    }
}