using NUnit.Framework;

namespace Core.NUnit
{
    [TestFixture]
    internal sealed class TeamStatsFixture
    {
        [Test]
        public void ItIsPossibleToCalculateTheSimilarityBetweenTwoStatistics()
        {
            var stat = new TeamStats(15, 30, 8, 10, 5, 5, 0);

            var similarStat = new TeamStats(18, 33, 7, 9, 5, 4, 0);
            var notSimilarStat = new TeamStats(5, 2, 35, 10, 0, 5, 5);

            var similarity1 = stat.GetSimilarity(similarStat);
            var similarity2 = stat.GetSimilarity(notSimilarStat);

            Assert.That(similarity1, Is.LessThan(similarity2));
        }

        [Test]
        public void SimilarityIsCommutative()
        {
            var stat = new TeamStats(15, 30, 8, 10, 5, 5, 0);
            var otherStat = new TeamStats(18, 33, 7, 9, 5, 4, 0);

            Assert.That(stat.GetSimilarity(otherStat), Is.EqualTo(otherStat.GetSimilarity(stat)));
        }
    }
}