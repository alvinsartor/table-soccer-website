﻿using System;
using Newtonsoft.Json;

namespace Core
{
    public sealed class Player
    {
        /// <summary>
        /// The player identifier.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// The player name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="name">The player name.</param>
        public Player(string name)
        {
            Id = Guid.NewGuid().ToString();
            Name = name;
        }

        [JsonConstructor]
        private Player(string id, string name)
        {
            Id = id;
            Name = name;
        }

        /// <inheritdoc />
        public override bool Equals(object? obj) =>
            obj is Player otherPlayer && otherPlayer.Id == Id && otherPlayer.Name == Name;

        /// <inheritdoc />
        public override int GetHashCode() =>
            HashCode.Combine(Id, Name);

        /// <inheritdoc />
        public override string ToString() 
            => Name;
    }
}
