﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Newtonsoft.Json;

namespace Persistence
{
    public sealed class MatchPersistence : FilePersistence
    {
        private readonly Dictionary<string, Match> _matches;

        /// <inheritdoc />
        protected override string GetFileName() => "Matches";

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchPersistence"/> class.
        /// </summary>
        public MatchPersistence(bool testMode = false)
            : base(testMode)
        {
            _matches = ReadData().Select(GetDetails).ToDictionary(x => x.Id, x => x);
        }

        /// <summary>
        /// Fetches all stored teams.
        /// </summary>
        public IEnumerable<Match> FetchAllMatches() => _matches.Values;

        /// <summary>
        /// Stores the new match.
        /// </summary>
        /// <param name="match">The match to store.</param>
        /// <exception cref="System.Exception">Match with id {match.Id} already exists.</exception>
        public void StoreMatch(Match match)
        {
            if (_matches.ContainsKey(match.Id))
            {
                throw new Exception($"Match with id {match.Id} already exists.");
            }

            _matches.Add(match.Id, match);
            StoreNewData(GetLine(match));
        }

        /// <summary>
        /// Removes the match.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <exception cref="System.Exception">Match with id {match.Id} does not exist.</exception>
        public void RemoveMatch(Match match)
        {
            if (!_matches.ContainsKey(match.Id))
            {
                throw new Exception($"Match with id {match.Id} does not exist.");
            }

            _matches.Remove(match.Id);
            StoreAllData(_matches.Values.Select(GetLine).ToArray());
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public new void Dispose()
        {
            base.Dispose();
        }

        private static string GetLine(Match details) => JsonConvert.SerializeObject(details);

        private static Match GetDetails(string line) => JsonConvert.DeserializeObject<Match>(line);
    }
}
