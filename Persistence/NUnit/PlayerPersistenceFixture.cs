using System;
using System.Linq;
using Core;
using NUnit.Framework;

namespace Persistence.NUnit
{
    [TestFixture]
    internal sealed class PlayerPersistenceFixture
    {
        private PlayerPersistence? _persistence;

        [SetUp]
        public void SetUp()
        {
            _persistence = new PlayerPersistence(true);
        }

        [TearDown]
        public void TearDown()
        {
            _persistence?.Dispose();
        }

        [Test]
        public void PlayersAreStoredAndRetrieved()
        {
            var player1 = new Player("Giacomo");
            var player2 = new Player("Kees");
            var player3 = new Player("Martha");

            _persistence!.StorePlayer(player1);
            _persistence.StorePlayer(player2);
            _persistence.StorePlayer(player3);

            CollectionAssert.AreEquivalent(new []{player1, player2, player3}, _persistence.FetchAllPlayers());
        }

        [Test]
        public void AddingSamePlayerTwiceCausesException()
        {
            var player1 = new Player("Giacomo");

            Assert.DoesNotThrow(() => _persistence!.StorePlayer(player1));
            Assert.Throws<Exception>(() => _persistence!.StorePlayer(player1));
        }

        [Test]
        public void PlayersCanBeDeleted()
        {
            var player1 = new Player("Giacomo");

            _persistence!.StorePlayer(player1);
            _persistence.RemovePlayer(player1);

            Assert.That(_persistence.FetchAllPlayers().Count(), Is.EqualTo(0));
        }
    }
}