using System;
using System.Linq;
using Core;
using NUnit.Framework;

namespace Persistence.NUnit
{
    [TestFixture]
    internal sealed class TeamPersistenceFixture
    {
        private TeamPersistence? _persistence;

        [SetUp]
        public void SetUp()
        {
            _persistence = new TeamPersistence(true);
        }

        [TearDown]
        public void TearDown()
        {
            _persistence?.Dispose();
        }

        [Test]
        public void TeamsAreStoredAndRetrieved()
        {
            var team1 = new Team(new[] {new Player("Juan"), new Player("Luis")});
            var team2 = new Team(new[] {new Player("Betty")}, "SuperBetty");
            
            _persistence!.StoreTeam(team1);
            _persistence!.StoreTeam(team2);

            CollectionAssert.AreEquivalent(new[] { team1, team2 }, _persistence.FetchAllTeams());
        }

        [Test]
        public void AddingSameTeamTwiceCausesException()
        {
            var team1 = new Team(new[] { new Player("Juan"), new Player("Luis") });

            Assert.DoesNotThrow(() => _persistence!.StoreTeam(team1));
            Assert.Throws<Exception>(() => _persistence!.StoreTeam(team1));
        }


        [Test]
        public void MatchesCanBeDeleted()
        {
            var team1 = new Team(new[] { new Player("Juan"), new Player("Luis") });
            
            _persistence!.StoreTeam(team1);
            _persistence.RemoveTeam(team1);

            Assert.That(_persistence.FetchAllTeams().Count(), Is.EqualTo(0));
        }
    }
}