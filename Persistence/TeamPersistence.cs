﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Newtonsoft.Json;

namespace Persistence
{
    public sealed class TeamPersistence : FilePersistence
    {
        private readonly Dictionary<string, Team> _teams;

        /// <summary>
        /// Gets the name of the file where the information is stored.
        /// </summary>
        /// <returns></returns>
        protected override string GetFileName() => "Teams";

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamPersistence"/> class.
        /// </summary>
        public TeamPersistence(bool testMode = false)
            : base(testMode)
        {
            _teams = ReadData().Select(GetDetails).ToDictionary(x => x.Id, x => x);
        }

        /// <summary>
        /// Fetches all stored teams.
        /// </summary>
        public IEnumerable<Team> FetchAllTeams() => _teams.Values;

        /// <summary>
        /// Stores the new team.
        /// </summary>
        /// <param name="team">The team to store.</param>
        /// <exception cref="System.Exception">Team with id {team.Id} already exists.</exception>
        public void StoreTeam(Team team)
        {
            if (_teams.ContainsKey(team.Id))
            {
                throw new Exception($"Team with id {team.Id} already exists.");
            }

            _teams.Add(team.Id, team);
            StoreNewData(GetLine(team));
        }

        /// <summary>
        /// Removes the team.
        /// </summary>
        /// <param name="team">The team.</param>
        /// <exception cref="System.Exception">Team with id {team.Id} does not exist.</exception>
        public void RemoveTeam(Team team)
        {
            if (!_teams.ContainsKey(team.Id))
            {
                throw new Exception($"Team with id {team.Id} does not exist.");
            }

            _teams.Remove(team.Id);
            StoreAllData(_teams.Values.Select(GetLine).ToArray());
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public new void Dispose()
        {
            base.Dispose();
        }

        private static string GetLine(Team details) => JsonConvert.SerializeObject(details);

        private static Team GetDetails(string line) => JsonConvert.DeserializeObject<Team>(line);
    }
}
