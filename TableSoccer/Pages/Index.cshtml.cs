﻿using System.Collections.Generic;
using System.Linq;
using Core;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace TableSoccer.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        /// <summary>
        /// All the registered players.
        /// </summary>
        public List<Player> Players => SoccerTableManager.Instance().Players();

        /// <summary>
        /// All the registered teams.
        /// </summary>
        public List<Team> Teams => SoccerTableManager.Instance().Teams();

        /// <summary>
        /// Initializes a new instance of the <see cref="IndexModel"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public IndexModel(ILogger<IndexModel> logger) => _logger = logger;

        /// <summary>
        /// Called when [post player].
        /// </summary>
        /// <param name="playerName">Name of the player.</param>
        public void OnPostPlayer(string playerName)
        {
            if (string.IsNullOrEmpty(playerName)) return;
            if (Players.Any(x => x.Name == playerName))
            {
                _logger.Log(LogLevel.Warning, "Player creation skipped as name is not unique.");
                return;
            }

            SoccerTableManager.Instance().AddNewPlayer(playerName);
            return;
        }

        /// <summary>
        /// Called when [post player delete].
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void OnPostPlayerDelete(string id)
        {
            SoccerTableManager.Instance().DeletePlayer(id);
        }

        /// <summary>
        /// Called when [post team].
        /// </summary>
        public void OnPostTeam()
        {
            var players = new string[]
                {
                    Request.Form["First Player"],
                    Request.Form["Second Player"],
                    Request.Form["Third Player"]
                }
                .Where(x => !string.IsNullOrEmpty(x))
                .Select(x => Players.FirstOrDefault(y => y.Name == x))
                .ToHashSet();
            
            if (players.Count == 0) return;
            
            if (TeamAlreadyExists(players))
            {
                _logger.Log(LogLevel.Warning, "Team creation skipped as a team with the same members already exists.");
                return;
            }

            SoccerTableManager.Instance().AddNewTeam(players, Request.Form["teamName"]);
        }

        /// <summary>
        /// Called when [post team delete].
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void OnPostTeamDelete(string id)
        {
            SoccerTableManager.Instance().DeleteTeam(id);
        }

        private bool TeamAlreadyExists(IEnumerable<Player> players)
        {
            var sortedPlayers = players.Select(x => x.Name).OrderBy(x => x).ToList();
            return Teams.Any(x => x.Members.Select(y => y.Name).OrderBy(z => z).SequenceEqual(sortedPlayers));
        }
    }
}