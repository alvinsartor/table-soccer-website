﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace TableSoccer.Pages
{
    public class MatchesModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        /// <summary>
        /// Gets the registered teams.
        /// </summary>
        public List<Team> Teams => SoccerTableManager.Instance().Teams();

        /// <summary>
        /// Gets the registered matches.
        /// </summary>
        public List<Match> Matches => SoccerTableManager.Instance().Matches();

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchesModel"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public MatchesModel(ILogger<IndexModel> logger) => _logger = logger;

        /// <summary>
        /// Called when [post match].
        /// </summary>
        public void OnPostMatch()
        {
            var homeTeam = Teams.Single(x => string.Equals(x.Name, Request.Form["Home Team"]));
            var visitorTeam = Teams.Single(x => string.Equals(x.Name, Request.Form["Visitor Team"]));
            var homeScore = int.Parse(Request.Form["homeScore"]);
            var visitorScore = int.Parse(Request.Form["visitorScore"]);
            var date = Convert.ToDateTime(Request.Form["matchDate"]);

            if (Equals(homeTeam, visitorTeam))
            {
                _logger.Log(LogLevel.Warning, "Match creation skipped because home and visitor are the same team.");
                return;
            }

            if (homeTeam.Members.Any(x => visitorTeam.Members.Contains(x))
                || visitorTeam.Members.Any(x => homeTeam.Members.Contains(x)))
            {
                _logger.Log(LogLevel.Warning, "Match creation skipped because one or more members is part of both teams.");
                return;
            }

            if (homeScore < 0 || visitorScore < 0)
            {
                _logger.Log(LogLevel.Warning, "Match creation skipped because of invalid score (must be greater than 0).");
                return;
            }

            SoccerTableManager.Instance().AddMatch(homeTeam, visitorTeam, homeScore, visitorScore, date);
        }

        /// <summary>
        /// Called when [post match delete].
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void OnPostMatchDelete(string id)
        {
            SoccerTableManager.Instance().DeleteMatch(id);
        }
    }
}