﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Persistence;

namespace TableSoccer
{
    public sealed class SoccerTableManager
    {
        private static SoccerTableManager _instance;

        private readonly PlayerPersistence _playerPersistence;
        private readonly TeamPersistence _teamPersistence;
        private readonly MatchPersistence _matchPersistence;

        private Statistics _statistics;

        /// <summary>
        /// Gets the current instance.
        /// </summary>
        public static SoccerTableManager Instance() => _instance ??= new SoccerTableManager();

        private SoccerTableManager()
        {
            _playerPersistence = new PlayerPersistence();
            _teamPersistence = new TeamPersistence();
            _matchPersistence = new MatchPersistence();

            _statistics = InitializeStatistics();
        }

        private Statistics InitializeStatistics()
        {
            var statistics = new Statistics();
            foreach (var team in _teamPersistence.FetchAllTeams())
            {
                statistics.AddTeam(team);
            }
            foreach (var match in _matchPersistence.FetchAllMatches())
            {
                statistics.AddMatch(match);
            }

            return statistics;
        }

        /// <summary>
        /// The list of all players.
        /// </summary>
        public List<Player> Players() => _playerPersistence.FetchAllPlayers().OrderBy(x => x.Name).ToList();

        /// <summary>
        /// The list of all teams.
        /// </summary>
        public List<Team> Teams() => _teamPersistence.FetchAllTeams().OrderBy(x => x.Name).ToList();

        /// <summary>
        /// The list of all matches.
        /// </summary>
        public List<Match> Matches() => _matchPersistence.FetchAllMatches().OrderByDescending(x => x.Date).ToList();

        /// <summary>
        /// The Teams ranking.
        /// </summary>
        public List<(Team team, int points, int matches)> Ranking() => _statistics.GetRanking();

        /// <summary>
        /// The last week ranking.
        /// </summary>
        public List<(Team team, int points, int matches)> CurrentWeekRanking()
        {
            var (weekBeginning, weekEnding) = GetCurrentWeek();
            return _statistics.GetRanking(weekBeginning, weekEnding);
        }

        /// <summary>
        /// Gets the overall statistics for the entire tournament.
        /// </summary>
        public TeamStats OverallStats() => _statistics.CalculateOverallStatistics();

        /// <summary>
        /// Gets the current week.
        /// </summary>
        public (DateTime weekBeginning, DateTime weekEnding) GetCurrentWeek()
        {
            var date = DateTime.Today;
            while (date.DayOfWeek != DayOfWeek.Monday) date = date.AddDays(-1);

            return (date, DateTime.Today);
        }

        /// <summary>
        /// Gets the overall statistics for the entire tournament.
        /// </summary>
        public TeamStats CurrentWeekStats()
        {
            var (weekBeginning, weekEnding) = GetCurrentWeek();
            return _statistics.CalculateOverallStatistics(weekBeginning, weekEnding);
        }

        /// <summary>
        /// Determines whether the team has statistics to show for the specified time span.
        /// </summary>
        /// <param name="team">The team.</param>
        public bool TeamHasStats(Team team) => _statistics.TeamHasStatisticsForTheSelectedTimeSpan(team, null, null);

        /// <summary>
        /// Gets the team statistics for the entire tournament.
        /// </summary>
        /// <param name="team">The team.</param>
        public TeamStats TeamStats(Team team) => _statistics.CalculateStatistics(team);

        /// <summary>
        /// Determines whether the team has statistics to show for the specified time span.
        /// </summary>
        /// <param name="team">The team.</param>
        public bool TeamHasStatsForCurrentWeek(Team team)
        {
            var (weekBeginning, weekEnding) = GetCurrentWeek();
            return _statistics.TeamHasStatisticsForTheSelectedTimeSpan(team, weekBeginning, weekEnding);
        }

        /// <summary>
        /// Gets the team statistics until last week.
        /// </summary>
        /// <param name="team">The team.</param>
        public TeamStats CurrentWeekTeamStats(Team team)
        {
            var (weekBeginning, weekEnding) = GetCurrentWeek();
            return _statistics.CalculateStatistics(team, weekBeginning, weekEnding);
        }

        /// <summary>
        /// Gets the daily progress for the specified team.
        /// </summary>
        /// <param name="team">The team to check.</param>
        public (DateTime date, int points)[] DailyProgress(Team team) =>
                    _statistics.GetDailyProgress(team);

        /// <summary>
        /// Gets the daily progress for the specified team, relative to the current week.
        /// </summary>
        /// <param name="team">The team to check.</param>
        public (DateTime date, int points)[] CurrentWeekProgress(Team team)
        {
            var (weekBeginning, weekEnding) = GetCurrentWeek();
            return _statistics.GetDailyProgress(team, weekBeginning, weekEnding);
        }

        /// <summary>
        /// Adds a new player.
        /// </summary>
        /// <param name="name">The name.</param>
        public void AddNewPlayer(string name)
        {
            var player = new Player(name);
            _playerPersistence.StorePlayer(player);
        }

        /// <summary>
        /// Deletes the player.
        /// </summary>
        /// <param name="playerId">The player identifier.</param>
        public void DeletePlayer(string playerId)
        {
            var player = Players().Single(x => x.Id == playerId);
            var teamsWithPlayer = _teamPersistence.FetchAllTeams().Where(x => x.Members.Any(y => y.Id == playerId));
            var matchesWithTeams = _matchPersistence.FetchAllMatches().Where(x => teamsWithPlayer.Any(x.Contains));

            foreach (var match in matchesWithTeams) _matchPersistence.RemoveMatch(match);
            foreach (var team in teamsWithPlayer) _teamPersistence.RemoveTeam(team);
            _playerPersistence.RemovePlayer(player);

            _statistics = InitializeStatistics();
        }

        /// <summary>
        /// Adds a new team.
        /// </summary>
        /// <param name="players">The team members.</param>
        /// <param name="name">The team name. Can be left empty (or null).</param>
        public void AddNewTeam(IEnumerable<Player> players, string name = "")
        {
            var team = new Team(players, name);
            _teamPersistence.StoreTeam(team);
            _statistics.AddTeam(team);
        }

        /// <summary>
        /// Deletes the team.
        /// </summary>
        /// <param name="teamId">The team identifier.</param>
        public void DeleteTeam(string teamId)
        {
            var team = _teamPersistence.FetchAllTeams().Single(x => x.Id == teamId);
            var matchesWithTeams = _matchPersistence.FetchAllMatches().Where(x => x.Contains(team));

            foreach (var match in matchesWithTeams) _matchPersistence.RemoveMatch(match);
            _teamPersistence.RemoveTeam(team);

            _statistics = InitializeStatistics();
        }

        /// <summary>
        /// Adds the match.
        /// </summary>
        /// <param name="homeTeam">The home team.</param>
        /// <param name="visitorTeam">The visitor team.</param>
        /// <param name="homeScore">The home score.</param>
        /// <param name="visitorScore">The visitor score.</param>
        /// <param name="date">The date this match has been played.</param>
        public void AddMatch(Team homeTeam, Team visitorTeam, int homeScore, int visitorScore, DateTime? date)
        {
            var match = new Match(homeTeam, visitorTeam, homeScore, visitorScore, date);
            _matchPersistence.StoreMatch(match);
            _statistics.AddMatch(match);
        }

        /// <summary>
        /// Deletes the match.
        /// </summary>
        /// <param name="matchId">The match identifier.</param>
        public void DeleteMatch(string matchId)
        {
            var match = _matchPersistence.FetchAllMatches().Single(x => x.Id == matchId);
            _matchPersistence.RemoveMatch(match);

            _statistics = InitializeStatistics();
        }
    }
}
